const orderUSPs = 'https://cdn.shopify.com/s/files/1/0544/5641/8501/t/47/assets/new_7.png?v=14521859693175754659';
const uspIcon1 = 'https://www.hetisaan.net/clients/dl/os691.png';
const uspIcon2 = 'https://www.hetisaan.net/clients/dl/os690.png';
const uspIcon3 = 'https://www.hetisaan.net/clients/dl/os689.png';
const uspIcon4 = 'https://www.hetisaan.net/clients/dl/os688.png';
const uspIcon5 = 'https://www.hetisaan.net/clients/dl/os687.png';
const uspIcon6 = 'https://www.hetisaan.net/clients/dl/os686.png';

const insertElements = nav => {
	const storeUSP = document.createElement('div');
	const storeUSPImage = document.createElement('img');
	storeUSPImage.src = orderUSPs;
	storeUSP.append(storeUSPImage);
	storeUSP.style.padding = '0 15px';
	storeUSP.style.margin = '10px 10px 0 10px';
	storeUSP.style.border = '1px solid #f0f0f0';
	nav.insertBefore(storeUSP, document.getElementById('MobileNav'));
}

const changeLIClasses = wrapper => {
	const listItems = wrapper.querySelectorAll('ul > li');
	let applyChange = false;
	listItems.forEach((listItem, index) => {
		const listLinkLabel = listItem.querySelector('.mobile-nav__label');
		if (!listLinkLabel) {
			applyChange = false;
		}
		if (applyChange) {
			listItem.classList.add(`ab-usp-li-${index}`);
		}
		if (listLinkLabel && listLinkLabel.textContent === 'Extras') {
			applyChange = true;
		}
	})
}

const addContactItem = wrapper => {
	const list = wrapper.querySelector('ul.mobile-nav');
	const extraItem = document.createElement('li');
	extraItem.classList.add('mobile-nav__item');
	extraItem.classList.add('ab-usp-li-12');
	extraItem.innerHTML = '<a href="/pages/kontakt" class="mobile-nav__link"><span class="mobile-nav__label">Kontakt</span></a>';
	list.append(extraItem);
}

/**
 * Apply styling constant in style tag to DOM
 */
const applyStyling = (styleString) => {
	const head = document.head || document.getElementsByTagName('head')[0];
	let style = document.createElement('style');
	head.appendChild(style);
	style.type = 'text/css';
	style.appendChild(document.createTextNode(styleString));
}

const abStyling = `
@media only screen and (max-width: 749px) {
	.site-header {
		border-bottom: 0 !important;
	}
	
	li[class^="ab-usp-li-"],
	li[class*=" ab-usp-li-"] {
		width: 50%;
		display: inline-block;
		border-bottom: none !important;
	}
	
	li[class^="ab-usp-li-"] .mobile-nav__link,
	li[class*=" ab-usp-li-"] .mobile-nav__link {
		font-size: 12px;
		position: relative;
	}
	
	li[class^="ab-usp-li-"] .mobile-nav__link:before,
	li[class*=" ab-usp-li-"] .mobile-nav__link:before {
		content: '';
		display: block;
		width: 30px;
		height: 30px;
		position: absolute;
		background-size: cover;
		left: 0;
		top: 8px;
	} 
	
	li[class^="ab-usp-li-"]:nth-child(even) .mobile-nav__link,
	li[class*=" ab-usp-li-"]:nth-child(even) .mobile-nav__link {
		padding: 15px 0 15px 60px;
	}
	
	li[class^="ab-usp-li-"]:nth-child(even) .mobile-nav__link:before,
	li[class*=" ab-usp-li-"]:nth-child(even) .mobile-nav__link:before {
		left: 30px;
	}
	
	li[class^="ab-usp-li-"]:nth-child(odd) .mobile-nav__link,
	li[class*=" ab-usp-li-"]:nth-child(odd) .mobile-nav__link {
		padding: 15px 30px 15px 30px;
	}
	
	.mobile-nav {
		display: flex;
		flex-wrap: wrap;
	}
	
	.mobile-nav-wrapper:after {
		display:none !important;
	}
	
	.ab-usp-li-7 .mobile-nav__link:before {
		background-image: url('${uspIcon6}');
	}
	
	.ab-usp-li-8 .mobile-nav__link:before {
		background-image: url('${uspIcon5}');
	}
	
	.ab-usp-li-9 .mobile-nav__link:before {
		background-image: url('${uspIcon3}');
	}
	
	.ab-usp-li-10 .mobile-nav__link:before {
		background-image: url('${uspIcon2}');
	}
	
	.ab-usp-li-11 .mobile-nav__link:before {
		background-image: url('${uspIcon1}');
	}
	
	.ab-usp-li-12 .mobile-nav__link:before {
		background-image: url('${uspIcon4}');
	}
}
`;

const navWrapper = document.querySelector('.mobile-nav-wrapper');
if (navWrapper) {
	insertElements(navWrapper);
	changeLIClasses(navWrapper);
	addContactItem(navWrapper);
	applyStyling(abStyling);
}
