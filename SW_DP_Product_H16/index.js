const addToCartForm = document.querySelector('.product-form__item--submit');

const imgBlockList = `
<ul class="ab-img-block-list">
	<li>
		<img src="https://hetisaan.net/clients/dl/include1.jpg" width="80" height="80" alt="" />
		<span>Die ideale Leinwand für dein schönes Bild</span>
	</li>
	<li>
		<img src="https://hetisaan.net/clients/dl/include2.jpg" width="80" height="80" alt="" />
		<span>Du entscheidest! Möchtest du dein Bild einrahmen und aufhängen?</span>
	</li>
	<li>
		<img src="https://hetisaan.net/clients/dl/include3.jpg" width="80" height="80" alt="" />
		<span>Alle Pinsel die du brauchst<br>Dick, Dun, Fein, Hart</span>
	</li>
	<li>
		<img src="https://hetisaan.net/clients/dl/include4.jpg" width="80" height="80" alt="" />
		<span>Mit unsere Anleitung gelingt jeder Pinselstrich und jedes kleine Detail</span>
	</li>
	<li>
		<img src="https://hetisaan.net/clients/dl/include5.jpg" width="80" height="80" alt="" />
		<span>Wunderschöne Farben, die dein Bild erstrahlen lassen</span>
	</li>
	<li>
		<img src="https://hetisaan.net/clients/dl/include6.jpg" width="80" height="80" alt="" />
		<span>Du hast weniger Stress, kommst zur Ruhe und hast ein tolles Erlebnis</span>
	</li>
</ul>
`;

const sw_dp_product_h16_styles = `
	.ab-img-block-list {
		list-style: none;
		margin: 1rem 0 0 0;
		padding: 0;
		display: flex;
		flex-wrap: wrap;
	}
	
	.ab-img-block-list li {
		width: calc(50% - 1rem);
		margin-right: 1rem;
		margin-bottom: 1rem;
		display: flex;
		justify-content: flex-start;
		align-items: center;
		color: #808080;
		font-size: 12px;
		text-align: left;
	}
	
	.ab-img-block-list li img {
		margin-right: .5rem;
	}
	
	@media screen and (min-width: 750px) and (max-width: 1023px) {
		.ab-img-block-list li {
			width: 100%;
		}
	}
	
	@media screen and (max-width: 550px) {
		.ab-img-block-list li {
			width: 100%;
		}
	}
`;

/**
 * Apply styling constant in style tag to DOM
 */
const applyStyling = (styleString) => {
	const head = document.head || document.getElementsByTagName('head')[0];
	let style = document.createElement('style');
	head.appendChild(style);
	style.type = 'text/css';
	style.appendChild(document.createTextNode(styleString));
}

/**
 * Add elements to DOM using Add To Cart form as a reference
 * @param addForm
 */
const handleElementAddition = (addForm) => {
	const imageBlock = addForm.nextElementSibling;
	if (imageBlock.classList.contains('text-center')) {
		imageBlock.querySelector('img').remove();

		const imageHeading = document.createElement('h4');
		imageHeading.innerHTML = 'Sofort startbereit!<br>Das ist alles bei deine Bestellung dabei';
		imageHeading.style.color = '#808080';
		imageHeading.style.textAlign = 'left';
		imageHeading.style.margin = '2rem 0 0';
		imageBlock.append(imageHeading);

		const blockListWrapper = document.createElement('div');
		blockListWrapper.classList.add('ab-block-list-wrapper');
		blockListWrapper.innerHTML = imgBlockList;
		imageBlock.append(blockListWrapper);
		applyStyling(sw_dp_product_h16_styles);

		// remove duplicate block
		const targetHeadings = document.querySelectorAll('h2[data-mce-fragment="1"]');
		targetHeadings.forEach(targetHeading => {
			const targetTextNode = targetHeading.querySelector('span strong');
			if (targetTextNode) {
				const targetText = targetTextNode.textContent;
				if (targetText.startsWith('Was ist alles bei Deiner Bestellung dabei?')) {
					targetHeading.remove();
				}
				if (targetText.startsWith('Was ist alles bei deiner Bestellung dabei?')) {
					targetHeading.remove();
				}
			}
		})
	}
}

if (addToCartForm) handleElementAddition(addToCartForm);
