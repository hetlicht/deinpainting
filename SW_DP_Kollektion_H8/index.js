const abBestsellers = [
	{link: '/products/dein-wunschmotiv', type: 'sale'},
	{
		link: '/products/malen-nach-zahlen-pusteblumen',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-herzensbaum',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-magisch-bluhende-blume',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-abstrakte-weltkarte',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-lowe-in-dunklen-blumen',
		type: 'sale'
	},
	{link: '/products/brechende-wellen', type: 'bestseller'},
	{
		link: '/products/malen-nach-zahlen-die-flasche-des-meeres',
		type: 'bestseller'
	},
	{link: '/products/elefanten', type: 'bestseller'},
	{
		link: '/products/malen-nach-zahlen-buddha-mit-blume',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-bunte-pusteblumen',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-elefant-mit-farbe',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-bunter-lower',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-tiger-in-dunklen-blumen',
		type: 'bestseller'
	},
	{link: '/products/edle-blume', type: 'bestseller'},
	{
		link: '/products/malen-nach-zahlen-lieblicher-fuchs',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-nachdenklicher-schimpanse',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-die-schildkrote',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-elefanten-in-afrika',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-kaffee-am-steg',
		type: 'bestseller'
	},
	{
		link: '/products/premium-malen-nach-zahlen-pusteblumen',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-bunte-katze-nahaufnahme',
		type: 'bestseller'
	},
	{link: '/products/malen-nach-zahlen-hirsch-2', type: 'bestseller'},
	{
		link: '/products/malen-nach-zahlen-bunter-affe',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-strasse-zu-den-bergen',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-monde-im-wasser',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-fuchs-in-herbstblatter',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-set-gold-blaue-blatter',
		type: 'sale'
	},
	{link: '/products/malen-nach-zahlen-roter-van', type: 'bestseller'},
	{
		link: '/products/malen-nach-zahlen-fuchs-in-rosen',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-bunter-schmetterling',
		type: 'bestseller'
	},
	{
		link: '/products/malen-nach-zahlen-abendsonne-bei-ruhigem-meer',
		type: 'sale'
	}
];
const abNeu = [
	{link: '/products/malen-nach-zahlen-der-federlowe', type: 'neu'},
	{
		link: '/products/malen-nach-zahlen-der-tanz-mit-rotem-kleid',
		type: 'neu'
	},
	{link: '/products/malen-nach-zahlen-ernster-hirsch', type: 'neu'},
	{
		link: '/products/malen-nach-zahlen-tigerkatze-mit-feder',
		type: 'sale'
	},
	{link: '/products/malen-nach-zahlen-elefantenfamilie', type: 'neu'},
	{link: '/products/malen-nach-zahlen-astronaut', type: 'neu'},
	{
		link: '/products/malen-nach-zahlen-papa-wolf-mit-sohn',
		type: 'neu'
	},
	{link: '/products/malen-nach-zahlen-lustige-giraffe-1', type: 'sale'},
	{
		link: '/products/malen-nach-zahlen-farbenfroher-elefant',
		type: 'neu'
	},
	{
		link: '/products/malen-nach-zahlen-schildkroten-und-fische',
		type: 'neu'
	},
	{
		link: '/products/malen-nach-zahlen-ausblick-zum-eiffelturm',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-wunderschoner-berg-und-see',
		type: 'neu'
	},
	{
		link: '/products/malen-nach-zahlen-spaziergang-bei-nacht-1',
		type: 'sale'
	},
	{link: '/products/malen-nach-zahlen-eule-bei-nacht', type: 'neu'},
	{
		link: '/products/malen-nach-zahlen-farbenfroher-hirsch',
		type: 'neu'
	},
	{
		link: '/products/malen-nach-zahlen-boote-in-wunderschonem-see',
		type: 'neu'
	},
	{link: '/products/malen-nach-zahlen-magische-gasse', type: 'sale'},
	{link: '/products/malen-nach-zahlen-produkt_4_-new', type: 'neu'},
	{link: '/products/malen-nach-zahlen-produkt_3_-new', type: 'neu'},
	{link: '/products/malen-nach-zahlen-produkt_2_-new', type: 'sale'},
	{link: '/products/malen-nach-zahlen-produkt_1', type: 'neu'},
	{
		link: '/products/mahlen-nach-zahlen-weihnachtsmann-steigt-in-den-kamin',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-weihnachtsmann-am-dach',
		type: 'neu'
	},
	{
		link: '/products/malen-nach-zahlen-weihnachtsmann-im-wald',
		type: 'neu'
	},
	{
		link: '/products/malen-nach-zahlen-weihnachtsmann-mit-geschenksack',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-frohlicher-weihnachtsmann',
		type: 'neu'
	},
	{
		link: '/products/mahlen-nach-zahlen-weihnachtsmann-und-sein-schlitten',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-weihnachtsmann-mit-schneetieren',
		type: 'neu'
	},
	{
		link: '/products/malen-nach-zahlen-schneemann-vorm-haus',
		type: 'neu'
	},
	{
		link: '/products/malen-nach-zahlen-weihnachtsmann-mit-reh',
		type: 'neu'
	},
	{
		link: '/products/malen-nach-zahlen-weihnachtsmann-mit-sternen',
		type: 'sale'
	},
	{
		link: '/products/malen-nach-zahlen-weihnachtsmann-snow',
		type: 'neu'
	}
]
const abLiebling = [
	{
		link: '/products/dein-wunschmotiv',
		type: 'lieblingsprodukt'
	}
]
const abAnfangerFreundlich = [
	{
		link: '/products/malen-nach-zahlen-bunter-lower',
		type: 'anfängerfreundlich'
	},
	{
		link: '/products/edle-blume',
		type: 'anfängerfreundlich'
	},
	{
		link: '/products/malen-nach-zahlen-hirsch-2',
		type: 'anfängerfreundlich'
	},
	{
		link: '/products/malen-nach-zahlen-elefant-mit-farbe',
		type: 'anfängerfreundlich'
	},
	{
		link: '/products/malen-nach-zahlen-abendsonne-bei-ruhigem-meer',
		type: 'anfängerfreundlich'
	}
]
const abGeheimfavorit = [
	{
		link: '/products/malen-nach-zahlen-pusteblumen',
		type: 'geheimfavorit'
	}
]
const abGeschenkidee = [
	{
		link: '/products/malen-nach-zahlen-uberraschungspaket',
		type: 'geschenkidee'
	}
]
const abBeliebt = [
	{
		link: '/products/malen-nach-zahlen-lowe-in-dunklen-blumen',
		type: 'beliebt'
	},
	{
		link: '/products/malen-nach-zahlen-herzensbaum',
		type: 'beliebt'
	},
	{
		link: '/products/malen-nach-zahlen-magisch-bluhende-blume',
		type: 'beliebt'
	},
	{
		link: '/products/malen-nach-zahlen-abstrakte-weltkarte',
		type: 'beliebt'
	}
]
/**
 * Apply styling constant in style tag to DOM
 */
const applyStyling = (styleString) => {
	const head = document.head || document.getElementsByTagName('head')[0];
	let style = document.createElement('style');
	head.appendChild(style);
	style.type = 'text/css';
	style.appendChild(document.createTextNode(styleString));
}
const abStyling = `
    .ab-product-label {
        position: absolute;
        left: -15px;
        top: -15px;
        color: black;
        font-weight: bold;
        padding: .25rem .5rem;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        border-radius: 3px;
        font-size: 13px;
        min-width: 110px;
        text-transform: capitalize;
    }
    
    .ab-product-label--bestseller {
        background: #fdefb7;
    }
    
    .ab-product-label--neu {
        background: #a1b7c6;
    }
    
    .ab-product-label--lieblingsprodukt {
        background: #feccca;
    }
    
    .ab-product-label--anfängerfreundlich {
        background: #ace3a0;
    }
    
    .ab-product-label--geheimfavorit {
        background: #bbb5b0;
    }
    
    .ab-product-label--geschenkidee {
        background: #e3bda0;
    }
    
    .ab-product-label--beliebt {
        background: red;
        color: white;
    }
    
    .ab-product-label--sale {
        background: #fff;
        box-shadow: 1px 1px 3px rgba(0,0,0,.25);
    }
    
    .grid--view-items {
        overflow: visible;
    }
    
    @media screen and (max-width: 400px) {
        .ab-product-label {
            min-width: 80px;    
        }
    }
`;
/**
 * Return product match from one of the lists. Prioritized by likeliness a product matches.
 * @param productLink
 * @returns {string}
 */
const getProductType = (productLink) => {
	let productType = '';

	const productTypes = [
		abBestsellers,
		abNeu,
		abLiebling,
		abAnfangerFreundlich,
		abGeheimfavorit,
		abGeschenkidee,
		abBeliebt
	]

	// loop through productLists & underlying productItems, match end of link with item
	productTypes.forEach(productList=> {
		productList.forEach(productItem=> {
			if (productLink.endsWith(productItem.link)) {
				productType = productItem.type;
			}
		})
	})

	return productType;
}
/**
 * Returns random int between 1 and 3
 * @returns {number}
 */
const getRandNumber = () => {
	return Math.floor((Math.random() * 3) + 1)
}
/**
 * Add sale label to given element
 * @param element
 */
const addSaleLabel = element => {
	const saleLabel = document.createElement('div');
	saleLabel.classList.add('ab-product-label');
	saleLabel.classList.add('ab-product-label--sale');
	saleLabel.textContent = 'sale';
	element.appendChild(saleLabel);
}
/**
 * Create product label, check for current label (and remove if so), insert new product label.
 * @param productType
 * @param element
 */
const createProductLabel = (productType, element) => {
	const prodLabel = document.createElement('div');
	prodLabel.classList.add('ab-product-label');
	prodLabel.classList.add(`ab-product-label--${productType}`);
	prodLabel.textContent = productType;
	// check if labels are already present and remove if so
	const currentLabels = element.querySelectorAll('.ab-product-label');
	if (currentLabels.length) {
		currentLabels.forEach(currentLabel => {
			currentLabel.remove();
		})
	}
	element.appendChild(prodLabel);
	// Add sale label to random 1/3 of Neu and Bestseller items
	/*if (productType === 'neu' || productType === 'bestseller') {
		 const randomInt = getRandNumber();
		 if (randomInt === 1) {
			 addSaleLabel(element);
		 }
	 }*/
}
const detectNewProducts = () => {
	const observer = new MutationObserver(function (mutations_list) {
		mutations_list.forEach(function (mutation) {
			mutation.addedNodes.forEach(function (added_node) {
				if (added_node.nodeName === 'LI' && added_node.classList.contains('grid__item')) {
					const newProductElement = added_node.querySelector('.grid-view-item__link');
					const productType = getProductType(newProductElement.href);
					if (productType.length) createProductLabel(productType, newProductElement);
				}
			});
		});
	});
	observer.observe(document.querySelector(".grid--view-items"), {subtree: false, childList: true});
}
/**
 * Biem.
 */
const init = () => {
	const productsInView = document.querySelectorAll('.grid-view-item__link');
	productsInView.forEach(productInView => {
		const productType = getProductType(productInView.href);
		if (productType.length) createProductLabel(productType, productInView);
	})
	applyStyling(abStyling);
	detectNewProducts();
}
init();
