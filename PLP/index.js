/**
 * Change body class so lightbox will show
 */
const triggerLightbox = () => {
	document.body.classList.add('ab-lightbox--active');
}

/**
 * Remove body class to hide lightbox
 */
const closeLightbox = () => {
	document.body.classList.remove('ab-lightbox--active');
}

/**
 * Apply styling constant in style tag to DOM
 */
const applyStyling = (styleString) => {
	const head = document.head || document.getElementsByTagName('head')[0];
	let style = document.createElement('style');
	head.appendChild(style);
	style.type = 'text/css';
	style.appendChild(document.createTextNode(styleString));
}

// handleMiniCart = () => {
// 	jQuery.ajax({
// 		type: 'GET',
// 		url: '/cart.js',
// 		dataType: 'json',
// 		success: function (data) {
// 			console.log(data);
// 		}
// 	})
// }

/**
 * Add the selected varian ID to cart, update cart product amount and show notification
 * @param vid
 */
const handleAddToCart = vid => {
	jQuery.ajax({
		type: 'POST',
		url: '/cart/add.js',
		data: {
			quantity: 1,
			id: vid
		},
		dataType: 'json',
		success: function (data) {
			console.log('Item added to cart', data);
			const cartCountElement = document.querySelector('[data-cart-count]');
			let cartCount = parseInt(cartCountElement.textContent);
			cartCountElement.textContent = cartCount + 1;
			closeLightbox();
			const atcNotification = document.createElement('div');
			atcNotification.classList.add('ab__add-to-cart-notification');
			atcNotification.textContent = 'Zum Warenkorb hinzugefügt!';
			document.body.append(atcNotification);
			// handleMiniCart();
			window.setTimeout(() => {
				// atcNotification.remove();
				atcNotification.style.display = 'none';
			}, 5000)
		}
	})
}

/**
 * Add or reduce product price and return formatted price
 * @param priceString
 * @param add
 * @param amount
 * @returns {string}
 */
const updatePrice = (priceString, add, amount) => {
	const priceParts = priceString.split(',');
	if (add) {
		priceParts[0] = parseInt(priceParts[0]) + amount;
	} else {
		priceParts[0] = parseInt(priceParts[0]) - amount;
	}
	return priceParts.join();
}

/**
 * Convert product options to radio button set in lightbox, add add-to-bag button and attach event listeners
 * @param options
 * @param product
 */
const handleProductOptions = (options, product) => {
	const optionItems = options.querySelectorAll('li');
	const lightBoxOptions = document.querySelector('.ab__product-options');
	lightBoxOptions.classList.add('ab-radio-wrapper');

	optionItems.forEach((optionItem, index) => {
		const radioButton = document.createElement('input');
		radioButton.name = 'rahmen';
		radioButton.type = 'radio';
		radioButton.value = optionItem.dataset.vid;
		radioButton.id = optionItem.dataset.vid;
		if (index === 0) {
			radioButton.checked = true;
		}

		const radioLabel = document.createElement('label');
		radioLabel.htmlFor = optionItem.dataset.vid;
		radioLabel.textContent = optionItem.querySelector('span').textContent;

		lightBoxOptions.append(radioButton);
		lightBoxOptions.append(radioLabel);
	})

	document.querySelectorAll('.ab-radio-wrapper input').forEach(optionInput => {
		optionInput.addEventListener('change', (e) => {
			const priceBox = document.querySelector('.ab__product-header .price');
			const salePrice = priceBox.querySelector('.price-item--sale');
			const comparePrice = priceBox.querySelector('.price__compare .price-item--regular');
			let increasePrice = false;

			// increase or decrease price in popup, based on chosen option
			if (e.target.nextSibling.textContent === 'Mit Rahmen' && e.target.checked) {
				increasePrice = true;
			}

			const newSalePrice = updatePrice(salePrice.textContent, increasePrice, 15);
			const newComparePrice = updatePrice(comparePrice.textContent, increasePrice, 15);
			salePrice.textContent = newSalePrice;
			comparePrice.textContent = newComparePrice;
		})
	})

	document.querySelector('.ab__atb').addEventListener('click', (e) => {
		e.preventDefault();
		const selectedVid = lightBoxOptions.querySelector('input[name="rahmen"]:checked').value;
		handleAddToCart(selectedVid);
	})

	applyStyling(inputStyling);
}

/**
 * Gather the available options of a product after clicking the add to cart button
 * @param product
 */
const detectOptions = product => {
	let observer = new MutationObserver(function (mutations, me) {
		const productOptions = product.querySelector('.satcb_quick_buy-dropdown');
		if (productOptions) {
			handleProductOptions(productOptions, product);
			me.disconnect(); // stop observing
			return;
		}
	});

	observer.observe(document, {
		childList: true,
		subtree: true
	});
}

/**
 * Populate lightbox with product data
 * @param product
 */
const buildLightbox = (product) => {
	const lightBox = document.querySelector('.ab__lightbox');

	const lightBoxChildren = lightBox.querySelectorAll('*');
	if (lightBoxChildren.length) {
		lightBoxChildren.forEach(lightBoxChild => {
			lightBoxChild.remove();
		})
	}

	// Identify elements to clone
	const productTitle = product.querySelector('.product-card__title');
	const productRating = product.querySelector('.loox-rating');
	const productPrice = product.querySelector('dl.price');

	// Add close button
	const lightboxClose = document.createElement('button');
	lightboxClose.textContent = '×';
	lightboxClose.classList.add('ab__lightbox-close');
	lightBox.append(lightboxClose);

	// clone elements
	const productTitleCopy = productTitle.cloneNode(true);
	const productRatingCopy = productRating.cloneNode(true);
	const productPriceCopy = productPrice.cloneNode(true);

	// create header
	const productHeader = document.createElement('div');
	productHeader.classList.add('ab__product-header');
	productHeader.append(productPriceCopy);
	productHeader.append(productRatingCopy);

	// append cloned elements
	lightBox.append(productTitleCopy);
	lightBox.append(productHeader);

	// create and add holder for productOptions
	const productOptions = document.createElement('div');
	productOptions.classList.add('ab__product-options');
	lightBox.append(productOptions);

	// create and add product actions div
	const productActions = document.createElement('div');
	productActions.classList.add('ab__product-actions');
	const productATB = document.createElement('button');
	productATB.classList.add('ab__atb');
	productATB.textContent = 'In den Warenkorb';
	productActions.append(productATB);
	lightBox.append(productActions);
	detectOptions(product);

	// Handle event listeners of new elements
	lightboxClose.addEventListener('click', closeLightbox);
}

/**
 * Create lightbox element as a framework in DOM
 */
const addLightBoxElements = () => {
	const lightboxBackdrop = document.createElement('div');
	lightboxBackdrop.classList.add('ab__backdrop');
	document.body.append(lightboxBackdrop);

	const lightboxWrapper = document.createElement('div');
	lightboxWrapper.classList.add('ab__lightbox-wrapper');

	const lightbox = document.createElement('div');
	lightbox.classList.add('ab__lightbox');

	lightboxWrapper.append(lightbox);
	lightboxBackdrop.append(lightboxWrapper);
}

/**
 * Listen for clicks on quick buy buttons
 */
document.addEventListener('click', e => {
	// Handle click on cart button
	if (e.target.classList.contains('satcb_quick_buy') || e.target.closest('.satcb_quick_buy') !== null) {
		// only fire when triggered by a human
		if (e.isTrusted) {
			const parentLi = e.target.closest('.grid__item');
			triggerLightbox();
			buildLightbox(parentLi);
		}
	}

	// Handle click on lightbox backdrop
	if (e.target.classList.contains('ab__backdrop')) closeLightbox();
})


const abStyling = `
	:root {
		--usp-text: "Nur +15€";
	}

	.satcb_glyphicon-shopping-cart {
		pointer-events: none;
	}
	
	.ab__backdrop {
		display: none;
		justify-content: center;
		align-items: center;
		position: fixed;
		left: 0;
		top: 0;
		right: 0;
		bottom: 0;
		width: 100%;
		height: 100%;
		background: rgba(0,0,0,.5);
		z-index: 99999;
		cursor: pointer;
	}
	
	.ab-lightbox--active .ab__backdrop {
		display: flex;
	}
	
	.ab__lightbox-wrapper {
		width: 100%;
		max-width: 700px;
		margin: 0 auto;
	}
	
	.ab__lightbox {
		width: 100%;
		max-width: calc(100% - 2rem);
		padding: 2rem 4rem;
		margin: 1rem;
		background: white;
		min-height: 100px;
		cursor: default;
		position: relative;
	}
	
	.ab__lightbox-close {
		position: absolute;
		right: 10px;
		top: 10px;
		background: transparent;
		border: none;
		color: black;
		font-size: 2rem;
		line-height: .625;
		padding: 10px;
		cursor: pointer;
	}
	
	.ab__product-header {
		display:flex;
		justify-content: space-between;
	}
	
	.ab__product-header > * {
		width: 50%;
		flex-direction: row;
	}
	
	.ab__product-header .loox-rating {
		display: flex;
		justify-content: flex-end;
	}
	
	.ab__product-actions {
		text-align: center;
	}
	
	.ab__atb {
		background: var(--color-sale-text);
		color: white;
		font-weight: bold;
		font-size: 1rem;
		padding: 10px 20px;
		border: none;
	}
	
	.ab__add-to-cart-notification {
		display: inline-block;
		position: fixed;
		border: 1px solid #ccc;
		border-radius: 2px;
		background: rgba(210,243,199,.9);
		right: 30px;
		top: 20px;
		padding: 10px 20px;
		z-index: 999;
	}
	
	.grid-view-item {
		padding-bottom: 36px;
	}
	
	.grid-view-item .price__badges {
		position: absolute;
		left: 0;
		top: 0;
		margin-top: 0;
	}
	
	.satcb_quick_buy.satcb_qb_top_right {
		background: var(--color-btn-primary-darker);
		color: #fff;
		border-radius: 0 !important;
		bottom: 0 !important;
		left: 0 !important;
		right: 0 !important;
		top: auto !important;
		width: 100% !important;
		height: 36px !important;
		text-align: center;
		padding: 10px 0;
		opacity: 1 !important;
	}
	
	.satcb_quick_buy [data-handle]:before {
		content: 'In den Warenkorb';
	}
	
	.satcb_quick_buy .satcb_glyphicon {
		display:none;
	}
	
	
	.ab-radio-wrapper {
		display: flex;
		margin: 0 -5px;
		padding: 2rem 0;
	}
	
	.ab-radio-wrapper input[type="radio"] + label {
		display: block;
		flex: 1 1 auto;
		margin: 0 5px;
		width: 100%;
		text-align: center;
		padding: .5rem;
		background: #f7f7f7;
		transition: transform.2s, box-shadow .2s;
		position: relative;
	}
	
	.ab-radio-wrapper input[type="radio"]:checked + label {
		box-shadow: 0 2px 5px #82a582;
		transform: translateY(-2px);
	}	
	
	.ab-radio-wrapper input + label + input + label:after {
		content: var(--usp-text);
		display: inline-block;
		position: absolute;
		top: 0;
		right: 0;
		background: #e2ece5;
		color: #518562;
		font-size: 11px;
		padding: 2px 4px;
	}
	
	@media screen and (max-width: 800px) {
		.ab__product-header {
			flex-direction: row;
		}
		
		.ab__product-header > * {
			width: auto;
		}
		
		.ab__lightbox {
			padding: 1rem;
		}
		
		.ab-radio-wrapper input + label + input + label:after {
			top: -12px;
			right: -4px;
		}
	}
`;

const inputStyling = `
	.ab-radio-wrapper input[type="radio"] {
		position: absolute !important;
		left: 0 !important;
		opacity: 0.01 !important;
	}
`;

applyStyling(abStyling);
addLightBoxElements();
