const abStyling = `
:root {
	--usp-text: "Nur +15€";
}
.product-form__item input[type="radio"] {
	position: absolute;
    left: 0;
    opacity: 0.01;
}

.product-form__item .ab-radio-wrapper {
	display: flex;
	margin: 0 -5px;
}

.product-form__item input[type="radio"] + label {
	display: block;
	flex: 1 1 auto;
	margin: 5px;
	width: 100%;
	text-align: center;
	padding: .5rem;
	background: #f7f7f7;
	transition: transform.2s, box-shadow .2s;
	position: relative;
}

.product-form__item input[type="radio"]:checked + label {
	box-shadow: 0 2px 5px #82a582;
	transform: translateY(-2px);
}

.ab-radio-label.ab-radio-label--usp:after {
	content: var(--usp-text);
	display: inline-block;
	position: absolute;
	top: -5px;
	right: -5px;
	background: #e2ece5;
	color: #518562;
	font-size: 11px;
	padding: 2px 4px;
}

.product-form__controls-group .selector-wrapper.js.product-form__item {
	width: 100% !important;
	flex-basis: 100% !important;
}

.product-form__item > label span {
	font-size: 11px;
	display: inline-block;
	margin-left: .75rem;
}
`;

let root = document.documentElement;

/**
 * Apply styling constant in styletag to DOM
 */
const applyStyling = () => {
	const head = document.head || document.getElementsByTagName('head')[0];
	let style = document.createElement('style');
	head.appendChild(style);
	style.type = 'text/css';
	style.appendChild(document.createTextNode(abStyling));
}

/**
 * Converts targeted selects to radio buttons and handles events & logic
 * @param item
 */
const convertToRadio = (item) => {
	const selectBox = item.querySelector('select');
	const selectOptions = selectBox.querySelectorAll('option:enabled');
	let radioWrapper = document.createElement('div');
	radioWrapper.classList.add('ab-radio-wrapper');
	item.append(radioWrapper);

	if (selectOptions.length <= 2) {
		selectOptions.forEach((selectOption, index) => {
			// create and populate radiobutton
			let radioButton = document.createElement('input');
			radioButton.name = selectBox.id;
			radioButton.type = 'radio';
			radioButton.value = selectOption.value;
			radioButton.checked = selectOption.selected;
			radioButton.id = selectBox.id + index;

			// create and populate label for radiobutton
			let radioLabel = document.createElement('label');
			radioLabel.htmlFor = selectBox.id + index;
			radioLabel.textContent = selectOption.value;
			radioLabel.classList.add('ab-radio-label');

			// add USP class if applicable
			if (radioLabel.textContent === 'Mit Rahmen') {
				radioLabel.classList.add('ab-radio-label--usp')
			}

			// change option price if more colors are selected
			if (selectOption.selected && radioLabel.textContent === '36 Farben') {
				root.style.setProperty('--usp-text', '"Nur +20€"');
			}

			// add radiobutton + label to wrapper
			radioWrapper.append(radioButton);
			radioWrapper.append(radioLabel);
		})

		selectBox.style.display = 'none';
	}

	// Handle event listeners for radiobutton toggles
	document.querySelectorAll(`input[name="${selectBox.id}"]`).forEach(radioItem => {
		radioItem.addEventListener('change', (e) => {
			selectOptions.forEach(selectOption => {
				if (e.target.value === selectOption.value) {
					// set select option of hidden select
					selectOption.selected = true;
					selectBox.dispatchEvent(new Event('change'));

					// handle USP text
					if (e.target.value === '24 Farben') {
						root.style.setProperty('--usp-text', '"Nur +15€"');
					}
					if (e.target.value === '36 Farben') {
						root.style.setProperty('--usp-text', '"Nur +20€"');
					}
				}
			})
		})
	})
}

/**
 * Strips spaces from a string...
 * @param stringContent
 * @returns {String}
 */
const stripSpacesFromString = (stringContent) => {
	return stringContent.replace(/\s+/g, '');
}

/**
 * Add new lines to form item labels
 * @param formLabel
 * @param formLabelText
 */
const handleLabelText = (formLabel, formLabelText) => {
	// first clean up the label
	formLabel.textContent = formLabelText;

	let labelExtraElement = document.createElement('span');
	let extraTextContent = '(Bitte auswählen)';

	if (formLabelText === 'Größe') {
		extraTextContent = '(In 4 Größen erhältlich)'
	}

	labelExtraElement.textContent = extraTextContent;
	formLabel.append(labelExtraElement);
}

// Define parent
const controlsGroup = document.querySelectorAll('.product-form__controls-group');
// Define items
const controlItems = controlsGroup[0].querySelectorAll('.product-form__item');
// define element labels
const controlItemLabels = controlsGroup[0].querySelectorAll('.product-form__item > label');

// Convert items to radio buttons
controlItems.forEach((controlItem, index) => {
	convertToRadio(controlItem);
})

// modify form label texts
controlItemLabels.forEach(controlItemLabel => {
	const labelText = controlItemLabel.textContent;
	if (stripSpacesFromString(labelText) === 'Größe' ||
		stripSpacesFromString(labelText) === 'Farbenauswahl' ||
		stripSpacesFromString(labelText) === 'Rahmen') {
		handleLabelText(controlItemLabel, stripSpacesFromString(labelText));
	}
})
// Handle styling
applyStyling();
